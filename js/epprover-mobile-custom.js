/**
 * ui.tabs bug fix for double loading.
 */
$.widget( "ui.tabs", $.ui.tabs, {
    _createWidget: function( options, element ) {
        var page, delayedCreate, that = this;
        if ( $.mobile.page ) {
            page = $( element ).parents(":jqmData(role='page'), :mobile-page").first();
            if ( page.length > 0 && !page.hasClass( "ui-page-active" ) ) {
                delayedCreate = this._super;
                page.one( "pagebeforeshow", function() {
                    //if (window.console) console.log("pagebeforeshow: delayedCreate exec");
                    if (delayedCreate !== null) delayedCreate.call( that, options, element );
                    delayedCreate = null;
                });
                var wait = setInterval(function() {
                    if (delayedCreate === null) clearInterval(wait);
                    else {
                        if ( page.hasClass( "ui-page-active" ) ) {
                            if (delayedCreate !== null) delayedCreate.call( that, options, element );
                            delayedCreate = null;
                            clearInterval(wait);
                        }
                    }
                }, 200);
            } else {
                return this._super( options, element );
            }
        } else {
            return this._super( options, element );
        }
    }
});